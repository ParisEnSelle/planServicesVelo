#!/usr/bin/python3
import urllib.request
import json
import logging

# Bounds for the searches
bounds = [{
    'lat': 48.123641,
    'lng': 1.445509
}, {
    'lat': 49.241608,
    'lng': 3.559454
}]

# Property filters for bike shops and bike pumps
property_filters = {
    "raw_shop": [["\"shop\"=\"bicycle\"", "!\"service:bicycle:diy\""],
                 ["\"shop\"=\"bicycle\"", "\"service:bicycle:diy\"=\"no\""],
                 ["\"shop\"=\"sports\"", "\"sport\"~\"cycling\""]],
    "pumps": [["\"service:bicycle:pump\"=\"yes\"", "!\"shop\"", "\"amenity\"!=\"community_center\""],
              ["\"service:bicycle:tools\"=\"yes\"", "!\"shop\"", "\"amenity\"!=\"community_center\""],
              ["\"amenity\"=\"bicycle_repair_station\"", "!\"shop\""]],
    "workshop": [["\"shop\"=\"bicycle\"", "\"service:bicycle:diy\"=\"yes\""],
                 ["\"club\"=\"bicycle\"", "\"service:bicycle:diy\"=\"yes\""]]
}


def get_bounds():
    return "(" + str(bounds[0]['lat']) + "," + str(
        bounds[0]['lng']) + "," + str(bounds[1]['lat']) + "," + str(
            bounds[1]['lng']) + ")"


def get_osm_data(property_filters):
    # Build the URL
    property_filters_bounded = ""
    for node_filter in property_filters:
        property_filters_bounded = property_filters_bounded + "node"
        for property_filter in node_filter:
            property_filters_bounded = property_filters_bounded + "[" + property_filter + "]"
        property_filters_bounded = property_filters_bounded + get_bounds(
        ) + ";"

    url = "https://overpass-api.de/api/interpreter?data=[out:json][timeout:25][maxsize:10485760];(" + property_filters_bounded + ");out;&target=mapql"
    logging.info("Performing request to following URL " + url)

    # Perform the request
    try:
        request = urllib.request.urlopen(url)
        if request is not None and request.getcode() == 200:
            json_result = json.loads(
                request.read().decode('utf-8'))['elements']
            logging.info("Request returned {0} results".format(
                len(json_result)))
            return json_result
        else:
            return []
    except json.JSONDecodeError:
        logging.warn(
            "Request for URL {0} failed during JSON decoding".format(url))
        return []
    except urllib.error.HTTPError as e:
        logging.warn(
            "Request for URL {0} failed due to bad error code. Received {1} and reason {2} and following content : \r\n{3}".format(url, e.code, e.reason, e.read()))
        return []
    except Exception as e:
        logging.warn(
            "Request for URL {0} failed with following exception : {1}".format(url, e))
        return []


if __name__ == '__main__':
    logging.basicConfig(filename='exec.log',
                        format='%(asctime)s:%(levelname)s:%(message)s',
                        level=logging.INFO)

    # Get pump data from OSM and compare it with the saved data
    for type_element, property_filter in property_filters.items():
        logging.info("Starting data retrieval for " + type_element)
        data = get_osm_data(property_filter)
        if len(data) > 0:
            # Open the file and get its content, if it exists
            try:
                data_file = open(type_element + ".json",
                                 "r+",
                                 encoding="utf-8")
                old_data = json.load(data_file)
            except json.JSONDecodeError:
                old_data = []
            except FileNotFoundError:
                data_file = open(type_element + ".json",
                                 "w+",
                                 encoding="utf-8")
                old_data = []

            # If the data differ, update the file
            if data != old_data:
                data_file.seek(0)
                data_file.truncate(0)
                json.dump(data, data_file)
                logging.info(type_element + " data updated")
            else:
                logging.info(type_element + " data not updated")

            data_file.close()
