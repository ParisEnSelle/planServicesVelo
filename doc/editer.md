# Éditer les informations

La carte des services vélo de Paris en Selle utilise les données d'OpenStreetMap (OSM). OpenStreetMap est l'équivalent libre de Google Maps : tout le monde peut en modifier les données, sans devoir attendre la validation de la modification par une entreprise ou l'un de ses algorithmes. L'ensemble des données affichées sur la carte peuvent donc être modifiées par n'importe qui, permettant ainsi de profiter du travail de la communauté tout en encourageant les gens à participer au travail de cartographie.

L'édition des données sur la carte est ici assez simple puisqu'il ne s'agit que de modifier des noeuds et leurs attributs. Un noeud dans OpenStreetMap est un point dans l'espace qui peut représenter une multitude de choses : un feu de signalisation, un arbre, un potelet, un magasin, etc. Les attributs sont des propriétés des noeuds : pour un magasin, cela permettra par exemple de renseigner les horaires d'ouverture, les services en magasin, les marques vendues, etc.

## Créer un compte sur OpenStreetMap

Pour créer un compte sur OpenStreetMap, rien de plus simple. Il suffit de suivre [ce lien](https://www.openstreetmap.org/user/new) et d'y remplir les quelques informations demandées : adresse email, nom d'utilisateur et mot de passe. Vos futures modifications seront signées de votre nom d'utilisateur, votre adresse email n'étant pas publique.

Lisez puis validez les conditions d'utilisation et il ne vous restera plus qu'à valider votre adresse email grâce au mail reçu.

## Modifier les données de la carte Paris en Selle

### Modification OpenStreetMap

Pour modifier les données d'un noeud existant, rendez vous sur la carte des services de Paris en Selle, cliquez sur l'élément que vous vouez modifier. Un popup s'ouvre alors et il contient un lien "Éditer les infos", cliquez dessus.

Une nouvelle page s'ouvre et vous voilà sur le site OpenStreetMap. Le lien vous envoie directement vers l'interface d'édition, c'est pourquoi il vous sera demandé de vous connecter pour continuer. L'interface d'édition ressemble à l'image ci-après :

![Interface éditeur OpenStreetMap](interface.png)

Le panneau latéral gauche permet de voir et modifier les informations du noeud sélectionné, tandis qu'au centre est affichée la carte et le noeud actuellement sélectionné est mis en évidence.

On peut déjà voir certains attributs dans le panneau latéral comme le nom du magasin, les horaires d'ouverture et les services proposés. Pour avoir une vue plus complète de tous les attributs du noeud, on peut scroller vers le bas et ainsi obtenir la vue suivante.

![Vue avec tous les attributs](attributs.png)

Pour modifier un attribut existant, il suffit de cliquer dans la case qui contient la valeur actuelle et de faire la modification. Attention, certains attributs (comme les heures d'ouverture) attendent un certain format pour la valeur (voir plus bas). Pour ajouter un attribut, il suffit de cliquer sur le bouton "+" en bas du tableau. Dans la case la plus à gauche, tapez le nom de l'attribut et dans la case à droite sa valeur. Là aussi, les noms d'attributs prennent des valeurs bien précises (voir plus bas).

À partir du moment où au moins une modification a été faite, vous pouvez l'enregistrer. Pour le faire, cliquez sur le bouton en haut à droite de la carte. Le panneau latéral changera pour vous permettre de mettre une explication rapide des changements effectués avant de pouvoir enfin envoyer les modifications. Si vous avez un doute sur les modifications apportées (bon attribut modifié ? bon format de valeur ?), n'hésitez pas à cocher la case "Je souhaite que quelqu'un vérifie mes modifications" : un(e) membre de la communauté OpenStreetMap vérifiera votre proposition avant enregistrement.

![1ère étape sauvegarder](sauvegarder_1.png)
![2ème étape sauvegarder](sauvegarder_2.png)

### Attributs utilisés sur la carte Paris en Selle

#### Magasins

Les attributs suivants sont utilisés pour les magasins :

- `name` : nom du magasin (example "Cyclable Paris 16")
- `operator` : si le nom du magasin n'est pas rempli, on utilise le nom de l'exploitant à la place, si renseigné (dans l'exemple précédent, on aurait "Cyclable")
- `opening_hours` : horaires d'ouverture. Le format est expliqué [ici](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours) avec des exemples assez complets [là](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours#Exemples)
- `service:bicycle:retail` : attribut vrai/faux (valeurs `yes`/`no`) concernant la vente de vélo
- `service:bicycle:second_hand` : attribut vrai/faux (valeurs `yes`/`no`) concernant la vente de vélo d'occasion
- `service:bicycle:repair` : attribut vrai/faux (valeurs `yes`/`no`) concernant la réparation de vélo
- `service:bicycle:rental` : attribut vrai/faux (valeurs `yes`/`no`) concernant la location de vélo
- `contact:phone` : numéro de téléphone
- `email`, `contact:email` : adresse email de contact
- `website`, `contact:website` : site web

#### Pompes à vélo / borne d'auto réparation

Les attributs suivants sont utilisés pour les pompes à vélos et les bornes d'auto-réparation :

- `name` : nom de l'équipement (exemple "Pompe en libre service")
- `operator` : si le nom de l'équipement n'est pas rempli, on utilise le nom de l'exploitant à la place, si renseigné (dans l'exemple précédent on pourrait avoir "Mairie de Paris")
- `opening_hours` : horaires d'ouverture. Le format est expliqué [ici](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours) avec des exemples assez complets [là](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours#Exemples)
- `service:bicycle:pump` : attribut vrai/faux (valeurs `yes`/`no`) concernant la présence d'une pompe
- `service:bicycle:stand` : attribut vrai/faux (valeurs `yes`/`no`) concernant la présence d'un support pour le vélo
- `service:bicycle:tools` : attribut vrai/faux (valeurs `yes`/`no`) concernant la présence d'outils pour la réparation de vélos

#### Ateliers d'auto réparation

Les attributs suivants sont utilisés pour les ateliers d'auto-réparation :

- `name` : nom de l'équipement
- `operator` : si le nom de l'équipement n'est pas rempli, on utilise le nom de l'exploitant à la place, si renseigné
- `opening_hours` : horaires d'ouverture. Le format est expliqué [ici](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours) avec des exemples assez complets [là](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours#Exemples)
- `contact:phone` : numéro de téléphone
- `email`, `contact:email` : adresse email de contact
- `website`, `contact:website` : site web

#### Vélo-écoles

Pour les vélo-écoles, l'utilisation d'OpenStreetMap n'est pas encore possible puisque l'attribut pour définir qu'un noeud est un vélo-école n'existe pas encore ([voir discussion](https://wiki.openstreetmap.org/wiki/Talk:Proposed_features/More_service:bicycle:school)).

## Avancé

### Fonctionnement

La carte des services de Paris en Selle utilise les données OpenStreetMap en exécutant des requêtes de recherchede noeuds, qui correspondent à un certain ensemble de filtres. Ces requêtes sont effectuées via [Overpass Turbo](https://wiki.openstreetmap.org/wiki/FR:Overpass_turbo).

Pour éviter de saturer les serveurs en effectuant une requête à chaque fois qu'un visiteur se rend sur notre carte, les requêtes sont effectuées par un [script Python](../assets/data/main.py) qui enregistre le résultat des requêtes dans des fichiers sur notre serveur. C'est ensuite ces fichiers de résultats qui sont directement utilisés. Ce script est exécuté toutes les six heures, soit 4 requêtes par jour alors que la page est visitée plus de cent fois par jour.

### Filtres utilisés pour l'extraction des données

#### Magasins

Pour caractériser un magasin de vélo, les filtres suivants sont utilisés (format `"attribut"="valeur"`) :

- `"shop"="bicycle"` et absence de l'attribut `"service:bicycle:diy"`
- `"shop"="bicycle"` et `"service:bicycle:diy"="no"`
- `"shop"="sports"` et `"sport"` contient `"cycling"`

Les deux premiers permettent de différencier un magasin de vélo d'un atelier d'autoréparation (DIY = Do It Yourself = Faîtes-le vous-même) tandis que le troisième filtre permet de récupérer les grandes surfaces de sport qui ne vendent pas que des vélos mais ont un rayon vélo.

#### Pompes à vélo / borne d'auto réparation

Pour caractériser une pompe à vélo / borne d'auto-réparation (format `"attribut"="valeur"`) :

- `"amenity"="bicycle_repair_station"` et absence d'un attribut `"shop"`
- `"service:bicycle:pump"="yes"` et absence d'un attribut `"shop"` et absence de `"amenity"="community_center"`
- `"service:bicycle:tools"="yes"` et absence d'un attribut `"shop"` et absence de `"amenity"="community_center"`

Le premier filtre permet de récupérer les pompes et bornes d'auto-réparation avec le nom d'équipement corrrespondant, tout en excluant les noeuds par ailleurs indiqués comme magasins. Les deux filtres suivants permettent de compenser les noeuds dont le nom d'équipement n'est pas renseigné ou inexact mais qui ont les attributs attendus pour une pompe ou une borne d'auto-réparation, tout en excluant les lieux qui peuvent proposer ce même genre de service (magasins, local associatif).

#### Ateliers d'auto réparation

Pour caractériser un atelier d'auto-réparation (format `"attribut"="valeur"`) :

- `"shop"="bicycle"` et `"service:bicycle:diy"="yes"`
- `"club"="bicycle"` et `"service:bicycle:diy"="yes"`

On sélectionne donc les lieux proposant le service DIY (Do It Yourself) et qui sont soit des magasins, soit des locaux associatifs.
